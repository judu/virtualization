# Copyright 2018-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pagure [ pn=libosinfo pnv=${PNV} suffix=tar.xz ]
require python [ blacklist=2 multibuild=false ]
require meson

SUMMARY="Tools for managing the libosinfo database files"
HOMEPAGE+=" https://libosinfo.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.9.0]
    build+run:
        app-arch/libarchive[>=3.0.0]
        core/json-glib
        dev-libs/glib:2[>=2.44]
        dev-libs/libxml2:2.0[>=2.6.0]
        gnome-desktop/libsoup:2.4
        !dev-libs/libosinfo:1.0[<1.0.0] [[
            description = [ libosinfo tools reside in a separate package now ]
            resolution = uninstall-blocked-after
        ]]
    test:
        dev-python/pytest[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
"

src_prepare() {
    meson_src_prepare

    # Respect selected python abi
    edo sed \
        -e "s:'python3':'python$(python_get_abi)':g" \
        -i tests/meson.build
}

