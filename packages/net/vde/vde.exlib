# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require option-renames [ renames=[ 'ssl cryptcab' ] ]
require github [ user=virtualsquare project=${PN}-2 tag=v${PV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="${PN} is a virtual distributed ethernet emulator"
DESCRIPTION="
VDE: Virtual Distributed Ethernet. It creates the abstraction of a virtual
ethernet: a single vde can be accessed by virtual and real computers.
VDE is primarily useful for virtualisation software like kvm, qemu, bochs, uml etc.
"

UPSTREAM_DOCUMENTATION="http://wiki.virtualsquare.org/ [[ lang = en ]]"

SLOT="0"
LICENCES="GPL-2"
MYOPTIONS="
    cryptcab [[ description = [ Enable the virtual Distributed Ethernet encrypted cable manager ] ]]
    pcap [[ description = [ Enable the VDE packet dump plugin using libpcap for package capturing with pdump ] ]]
"

DEPENDENCIES="
    build+run:
        cryptcab? ( dev-libs/wolfssl )
        pcap? ( dev-libs/libpcap )
    run:
        group/vde
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-profile
    --enable-experimental
    --enable-tuntap
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    cryptcab
    pcap
)

